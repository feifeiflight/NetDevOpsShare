from jinja2 import Template

import pandas as pd

def get_meta_datas(file='vscode_user_sinppets.xlsx'):
    data = pd.read_excel(file)
    data = data.to_dict(orient='records')
    print(data)
    return data

def make_snippets_files():
    with open('code-snippets.j2','r',encoding='utf8') as f:
        templ = Template(f.read())
    meta_datas = get_meta_datas()
    for data in meta_datas:
        data['commands'] = data['template'].splitlines()
        with open('{}_{}_{}.code-snippets'.format(data['vendor'],data['platform'],data['name']),'w',encoding='utf8') as snippets_file:
            snippets_file.write(templ.render(**data))

if __name__ == "__main__":
    make_snippets_files()