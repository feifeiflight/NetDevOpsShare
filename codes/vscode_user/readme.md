# 网工专属vscode 用户代码片段生成器

## 思路

通过表格和脚本创建vscode用户代码片段，这样在vscode里就可以很快的输入一段标准的配置模板了，然后改吧改吧就可以用了。

## 使用

1. 在vscode_user_sinppets.xlsx中依次输入厂商、平台、代码片段名称，配置模板以及描述。
2. 调用脚本，生成代码片段配置文件
3. 将代码片段配置文件拷贝到vscode的用户代码片段全局的指定目录，比如我的是C:\Users\Administrator\AppData\Roaming\Code\User\snippets。
4. 打开vscode，可以各种随意输入字母，不必在乎顺序和完整性，vscode会自动筛选。比如我的一个代码片段是cisco_nxos_interface，我可以输入这个名称的任意字符且无需在意顺序

## 效果

![image-20200812162703787](assets/image-20200812162703787.png)

![image-20200812162817561](assets/image-20200812162817561.png)