from napalm import get_network_driver

'''
ansible_connection=network_cli
ansible_network_os=nxos
ansible_port=8181
ansible_user=admin
ansible_password=Admin_1234!
'''


def show_nxos_facts():
    driver = get_network_driver('ios')
    nxos_dev = driver('192.168.199.102', 'admin', 'admin123!', optional_args={'secret': 'admin123!'})
    nxos_dev.open()

    output = nxos_dev.get_facts()
    print(output)


def config_nxos():
    driver = get_network_driver('ios')
    nxos_dev = driver('192.168.199.102', 'admin', 'admin123!', optional_args={'secret': 'admin123!'})
    nxos_dev.open()

    config_string = """vlan 222
                              name napalm"""

    output = nxos_dev.load_merge_candidate(config=config_string)
    print(output)

    print('\ndiff\n')
    diff = nxos_dev.compare_config()
    print(diff)
    choice = input("\nWould you like to commit these changes? [yN]: ")
    if choice == 'y':
        print('Committing ...')
        nxos_dev.commit_config()
    else:
        print('Discarding ...')
        nxos_dev.discard_config()


if __name__ == '__main__':
    config_nxos()
