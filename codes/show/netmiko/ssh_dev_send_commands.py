from netmiko import ConnectHandler


def ssh_dev_exc(dev_type, ip, commands, port=22, username='readonly', password='readonly'):
    dev_info = {
        'device_type': dev_type,
        'ip': ip,
        'port': port,
        'username': username,
        'password': password
    }

    with ConnectHandler(**dev_info) as dev_connection:
        outputs = []
        for c in commands:
            output = dev_connection.send_command(c)
            outputs.append(output)

        return outputs

def ssh_dev_config(dev_type, ip, commands, port=22, username='readonly', password='readonly'):
    dev_info = {
        'device_type': dev_type,
        'ip': ip,
        'port': port,
        'username': username,
        'password': password
    }

    with ConnectHandler(**dev_info) as dev_connection:
        outputs = []
        output = dev_connection.send_config_set(config_commands=commands)
        outputs.append(output)
        output = dev_connection.save_config()
        outputs.append(output)
        return outputs

if __name__ == '__main__':
    some_dev_ssh_info = {
        'dev_type': 'cisco_ios',
        'ip': '192.168.199.102',
        'port': 22,
        'username': 'admin',
        'password': 'admin123!',
        'commands': ['show version']
    }
    outputs = ssh_dev_exc(**some_dev_ssh_info)
    print(outputs)

    some_dev_ssh_info['commands']= '''
    interface eth0
    description configed by netmiko
    '''.splitlines()
    outputs = ssh_dev_config(**some_dev_ssh_info)
    print(outputs)