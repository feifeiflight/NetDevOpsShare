from zet_textfsm import ZetTextFsm


def main(template, text):
    fsm = ZetTextFsm(template)
    version_info = fsm.parse_2_structured_data(text, multi=False)
    return version_info


if __name__ == '__main__':
    with open('show_version.log', 'r', encoding='utf8') as f:
        dev_text = f.read()
    version = main('show_version.textfsm_template', dev_text)

    print(version)
