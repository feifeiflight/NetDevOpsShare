from textfsm import TextFSM
from zet_textfsm import ZetTextFsm
def user_textfsm():
    fsm = TextFSM(open('show_version.textfsm_template'))

    with open('show_version.log','r',encoding='utf8') as f:
        dev_text = f.read()

    fsm_results = fsm.ParseText(dev_text)
    print(fsm_results)
    print(fsm.header)

def user_zet_textfsm():
    fsm = ZetTextFsm('show_version.textfsm_template')
    with open('show_version.log','r',encoding='utf8') as f:
        dev_text = f.read()
    data = fsm.parse_2_structured_data(dev_text,multi=False)
    print(data)

if __name__ == '__main__':
    user_textfsm()