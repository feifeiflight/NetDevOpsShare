from textfsm import TextFSM


if __name__ == '__main__':
    with open('show_int_bri.log', 'r', encoding='utf8') as f:
        dev_text = f.read()
        print(dev_text)
    template = TextFSM(open('show_interface_bri.textfsm'))
    version_info = template.ParseTextToDicts(dev_text)

    print(version_info)
