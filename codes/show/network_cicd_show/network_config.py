from nornir import InitNornir
from nornir.plugins.tasks.networking import netmiko_send_command, netmiko_send_config, netmiko_save_config
from nornir.plugins.functions.text import print_result


nr = InitNornir(
    # config_file="nornir.yaml"
)

result = nr.run(task=netmiko_send_command, command_string='show inte descr')
print_result(result)

result = nr.run(task=netmiko_send_config,
                config_commands=['interface  Et0',
                                 'description nornir is good!!',
                                 ]
                )

print_result(result)

result = nr.run(task=netmiko_save_config)

print_result(result)

result = nr.run(task=netmiko_send_command, command_string='show inte descr')
print_result(result)
