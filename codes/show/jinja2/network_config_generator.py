from jinja2 import Template

import pandas as pd


def get_config_datas(excel_file):
    data = pd.read_excel(excel_file)
    data = data.to_dict(orient='records')
    print(data)
    return data


def generate_configs_4_one_dev_in_rows(j2_file,excel_file,output_file='configs.txt'):
    with open(j2_file, 'r', encoding='utf8') as f:
        templ = Template(f.read())
    config_datas = get_config_datas(excel_file)
    with open(output_file, 'w',encoding='utf8') as configs_file:
            configs_file.write(templ.render(items=config_datas))
    print('done，everything is ok！')


if __name__ == "__main__":
    excel_file = 'interfaces.xlsx'
    j2_file = 'interfaces_desc.j2'
    generate_configs_4_one_dev_in_rows(j2_file,excel_file)
