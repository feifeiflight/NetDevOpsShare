用于演示个人认为值得一看的网络自动化的开源软件



从简单的工具包到自动化框架


- jinja2
- textfsm
- netmiko
- ntc-templates
- napalm （不认为适合国内行情）
- nornir 从小到中的自动化运维框架，个人认为非常优秀。观望中。本文演示用的是2.5.0版本，请知悉。其他的版本问题都不大，最新的即可。
- ansible 不适合国内行情，但是有些可取之处或者可复用的地方 
- pysnmp

- requests


一些学前知识：
- 正则表达式
- json
- yaml
- RESTful API
- netconf 与 yang model