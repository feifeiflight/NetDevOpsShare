from nornir import InitNornir
from nornir.plugins.functions.text import print_result


nr = InitNornir(
    # config_file="nornir.yaml", dry_run=True
)


def hi(task):
    print(f"hi! My name is {task.host.name}")


def say(task, sth):
    print(f"hi! My name is {task.host.name}, {sth}")


# results = nr.run(task=hi)
results = nr.run(task=say, sth='hello world!')
print_result(results)
