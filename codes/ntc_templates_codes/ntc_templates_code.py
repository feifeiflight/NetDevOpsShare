from ntc_templates.parse import parse_output

if __name__ == "__main__":
    with open('show_version.log', 'r', encoding='utf8') as f:
        raw_config_text = f.read()
    datas = parse_output(platform='cisco_nxos', command='show version', data=raw_config_text)
    print(datas)


    with open('show_int_bri.log', 'r', encoding='utf8') as f:
        raw_config_text = f.read()
    datas = parse_output(platform='cisco_nxos', command='show interface brief', data=raw_config_text)
    for i in datas:
        print(i)
