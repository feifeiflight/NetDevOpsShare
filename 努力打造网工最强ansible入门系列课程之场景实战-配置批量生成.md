# ansible实战之基于模板的批量配置生成

## 背景

网络运维，我们有很多时间是在准备配置的路上，咱们之前也讲过，从脑海中或者是从自己的宝藏笔记中找出模板，一顿操作猛如虎，Ctrl C+ Ctrl V。这个过程是十分危险的，因为人不是机器，肯定会出错，放纵自己去放空自己去复制粘贴，出的配置，早晚要“湿鞋”。

我们之前讲过用jinja2+python去生成配置，今天我们讲讲用ansible去生成配置，其底层实际也使用的是jinja2。但是借助于playbook和inventory文件（hosts），它可以帮助我们省去一些代码，同时可以将生成的配置结合ansible去实现更复杂的内容，比如推送到设备或者推送到git，比如将准备的配置在下一个ansible的playbook中使用，或者是产生的配置是下一个task的任务等等。

废话不多讲，我们来看看如何用ansible批量生成配置，以及其中的一些技巧。

其中主要涉及到的知识点是template模块的使用以及变量的定义。

我们演示的场景还是最简单的端口配置。看看如何快速生成端口配置。

jinja2模板引擎使用大家可以参考我之前的文章。

## 最简版：变量+jinja2模板

我们先来一个简单版本的。

简单版本首先要定义一个jinja2的模板文件，jinjia2的文件一般使用j2作为后缀。我们端口配置的j2如下：

```jinja2
{% for intf in  interfaces %}
{{ intf[0] }}
description {{ intf[1]}}
{% endfor %}
```

其中我们把端口定义为这样的一个数据格式 这个是在playbook中定义的。

```yaml
interfaces:
      - ['Ethr1/50','configed by ansible templates']
      - ['Ethr1/51','configed by ansible templates']
      - ['Ethr1/52','configed by ansible templates']
```

这里我们把每个端口定义成了列表，其实也可以定义成dict字典，二者各有利弊，列表写一行，看着舒坦，书写简单。dict的key会让整体可读性更好，这个看大家的取舍。我这里偷个懒。

其中变量interfaces的名字与j2文件中的变量必须同名，这个是ansible的对应模块帮我们去自动将interfaces与j2 render生成配置的，所以一定要同名。

然后我们来定义我们对应的把变量render到模板中的task了。

```yaml
 - name: 执行show命令
      template:
        src: interfaces.j2
        dest: /tmp/{{ansible_host}}.config
```

这个template模块，从上面的playbook来看很简单：

- src：是本地jinja2模板的路径。

- dest：这个从官方和网上的一些资料去看，都是remote server的路径。但是，针对网络设备，这是一个本地的路径。

还有一些和Linux文件相关的参数，是否覆盖，访问权限等，默认是覆盖的，我们不是很关注。

dest我们说是remote server的路径，这个其实描述的有点问题。对于host中的设备，如果是服务器，dest是远程服务器的路径，但是针对网络设备，这个dest是ansible主机（或者我们称之为控制器）的本地路径，这个大家要注意一下。

有兴趣大家可以去看看template的源代码，它是根据jinja2模板和变量生成配置，然后利用copy模块把配置复制到远端服务器，如果是网络设备，则会把内容创建在本地（这个是我根据实验获取一个结论，与connection的配置方式无关，在源代码中找这个逻辑找的也很模糊，作为网络运维人员，我们这样简单理解我觉得是没问题的）。

这个阶段的playbook：

```yaml
---
- name: 根据模板和变量生成配置文件01-变量内置到playbook
  hosts: all  # filter网络设备
  gather_facts: no # 收集设备的一些基础信息。比如端口、版本、平台等等，耗费时间，大家都选择不调用
  vars:
    interfaces:
      - ['Ethr1/50','configed by ansible templates'] 
      - ['Ethr1/51','configed by ansible templates'] 
      - ['Ethr1/52','configed by ansible templates'] 


  tasks:
    - name: 批量生成配置
      template:
        src: interfaces.j2
        dest: /tmp/{{ansible_host}}.config
```

结果：

![image-20200731190510702](assets/image-20200731190510702.png)

![image-20200731190552894](assets/image-20200731190552894.png)



## 升级版：变量文件+jinja2模板

刚才的方式我们生成了配置，但是变量都写在了一个playbook中，当端口比较多的时候，这个playbook就显得非常大，为了看实际运行的内容要拖好长才能看到。

这个时候我们可以简单升级一下，把变量提取到一个指定的yaml文件，然后通过yaml文件描述我们的配置参数。

首先我们简单定义一个要配置的端口的yaml文件。

interfaces.yml:

```yaml
---
interfaces:
      - ['Ethr1/50','configed by ansible templates'] 
      - ['Ethr1/51','configed by ansible templates'] 
      - ['Ethr1/52','configed by ansible templates'] 
```

定义好变量，我们把这个文件导入到playbook中.

```yaml
---
- name: 根据模板和变量生成配置文件01-变量内置到playbook
  hosts: all  # filter网络设备
  gather_facts: no # 收集设备的一些基础信息。比如端口、版本、平台等等，耗费时间，大家都选择不调用
  # 给内置变量var_files赋值我们的变量文件 
  vars_files:
    - interfaces.yml


  tasks:
    - name: 批量生成配置
      template:
        src: interfaces.j2
        dest: /tmp/{{ansible_host}}_var_file.config
```

结果：

![image-20200731191933541](assets/image-20200731191933541.png)

我们对内置变量vars_files进行了赋值，进而导入了我们的变量文件。我们可以导入多个变量文件。

如此，我们便可以将一个playbook固定下来，以后每次修改变量文件内容批量生成配置文件，且可以重复利用。

这种适合于一些大批量的配置生成。

## 脑洞大开

### 延伸拓展：

- 基于变量及文件配置自动生成，我们可以固化一些常用的应急场景，比如防火墙切换，将每组防火墙切换的变量文件准备好，然后在应急的时候去调用对应的变量文件

- 开局自动化
- 日常变更割接的模板管理、配置生成自动化及推送自动化

### 后续：

- 基于ansible，可以结合我们之前相关推送模块,Cli的或者是厂商自带的一些copy_config(从本地文件读取写入到设备
- 通过自己的paramiko与netmiko脚本去推送配置
- 用我们自己的配置推送工具去推送。一些大厂都会有自研或者购买的配置管理工具可以批量推送配置
- 手刷（最不推荐）



今天就到这里结束，以上只是一部分演示。我们可以利用ansible与jinja2批量生成配置。

下一次，我们讲讲基于角色的playbook的编写，可以与这个结合，按角色生成不同的配置。这种适用一些固定场景或者是重要变更或者是开局自动化等等

感觉这次划了水。下次分享计划是RESTful API （与这个ansible的交叉着来），这个系列打算拆成3-5次去分享，从http到RESTful API到requests模块使用详解，因为内容其实有点多。

欢迎大家继续关注、点赞、分享、喜欢、收藏、订阅，推荐给你身边的网工！

同名知乎专栏和微信公众号：**NetDevOps加油站**，欢迎你的加入！



